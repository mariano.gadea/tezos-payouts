<h1>tezos-payouts</h1>
<p>&nbsp;</p>
<div>This&nbsp;is&nbsp;a&nbsp;very&nbsp;simple&nbsp;tezos&nbsp;payout&nbsp;script&nbsp;built&nbsp;on&nbsp;python.&nbsp;</div>
<div>When&nbsp;executed&nbsp;it&nbsp;will&nbsp;request&nbsp;as&nbsp;input&nbsp;which&nbsp;cycle&nbsp;you&nbsp;want&nbsp;to&nbsp;pay&nbsp;then&nbsp;it&nbsp;query&nbsp;the&nbsp;rewards&nbsp;APIs&nbsp;from&nbsp;tzkt.io&nbsp;and&nbsp;calculate&nbsp;the&nbsp;amount&nbsp;to&nbsp;pay&nbsp;to&nbsp;each&nbsp;delegate&nbsp;by&nbsp;discounting&nbsp;the&nbsp;baker&nbsp;fee&nbsp;that&nbsp;was&nbsp;setup&nbsp;in&nbsp;the&nbsp;variables&nbsp;of&nbsp;the&nbsp;script.</div>
<div>This&nbsp;script&nbsp;creates&nbsp;a&nbsp;single&nbsp;bulk&nbsp;payment&nbsp;transaction&nbsp;and&nbsp;commit&nbsp;it&nbsp;to&nbsp;the&nbsp;blockchain&nbsp;once&nbsp;confirmed.</div>
<div>It&nbsp;also&nbsp;includes&nbsp;2&nbsp;database&nbsp;tables&nbsp;that&nbsp;I&nbsp;use&nbsp;to&nbsp;track&nbsp;the&nbsp;payments,&nbsp;you&nbsp;can&nbsp;simply&nbsp;comment&nbsp;the&nbsp;sql&nbsp;lines.</div>
<p>&nbsp;</p>
<h3>Requirements</h3>
<ul>
<li>Linux server</li>
<li>Python3&nbsp;installed</li>
<li>pip libraries requests, json, os, base58check, pymysql (pip install command)</li>
<li>A&nbsp;payment&nbsp;wallet&nbsp;must&nbsp;be&nbsp;declared&nbsp;locally&nbsp;on&nbsp;the&nbsp;server&nbsp;with&nbsp;enough&nbsp;balance&nbsp;to&nbsp;pay&nbsp;and&nbsp;defined&nbsp;as&nbsp;unencrypted</li>
<li>there&nbsp;is&nbsp;a&nbsp;limit&nbsp;of&nbsp;300&nbsp;delegators,&nbsp;if&nbsp;you&nbsp;have&nbsp;more&nbsp;than&nbsp;300&nbsp;delegators&nbsp;you&nbsp;should&nbsp;modify&nbsp;limit=300&nbsp;in&nbsp;url&nbsp;variable&nbsp;definition</li>
</ul>
<p>&nbsp;</p>
<div>This script writes on a database, you might want to adapt the script to write in your own database. If you want to skip this you need to comment rows that start with "conn", "sql" and "cursor"</div>
<p>&nbsp;</p>
<div>Also,&nbsp;this&nbsp;sript&nbsp;assumes&nbsp;that&nbsp;there&nbsp;is&nbsp;a&nbsp;running&nbsp;node&nbsp;on&nbsp;the&nbsp;machine,&nbsp;if&nbsp;you&nbsp;want&nbsp;to&nbsp;inject&nbsp;the&nbsp;operation&nbsp;into&nbsp;another&nbsp;node&nbsp;(ie.&nbsp;giganode)&nbsp;you&nbsp;should&nbsp;change&nbsp;the&nbsp;variable&nbsp;"red"</div>
<div>&nbsp;</div>
<div>This script pays to delegates that delegated at least 10 XTZ, you can modify the script to avoid or increase this.</div>
<div>&nbsp;</div>
<h3>Configure variables</h3>
<div>&nbsp;</div>
<div>
<pre class="code highlight" lang="python"><span id="LC21" class="line" lang="python"><span class="n">red: N</span><span class="c1">etwork were we will inject opertion (localhost if node is running)</span></span>
<span id="LC24" class="line" lang="python"><span class="n">redapi: API node no need to change</span><span class="s">'https://api.tzkt.io'</span> </span>
<span id="LC25" class="line" lang="python"><span class="n">sourceaddress: your baker address</span></span>
<span id="LC26" class="line" lang="python"><span class="n">payaddress: payout address that will pay</span></span>
<span id="LC27" class="line" lang="python"><span class="n">payalias: alias defined locally for payaddress</span></span>
<span id="LC28" class="line" lang="python"><span class="n">tablasql: only if you want to store results in a DB</span></span>
<span id="LC29" class="line" lang="python"><span class="n">tablaint: <span id="LC28" class="line" lang="python">only if you want to store results in a DB</span></span></span>
<span id="LC30" class="line" lang="python"><span class="n">gas_limit: </span><span class="s">"15385"</span></span>
<span id="LC31" class="line" lang="python"><span class="n">fee: "</span><span class="s">1792" you can test with a smaller amount </span></span>
<span id="LC32" class="line" lang="python"><span class="n">storage_limit: </span><span class="s">"300"</span></span>
</pre>
</div>
<h3>How to run</h3>
<div>python3 paytezos.py</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<h3>Known issues</h3>
<div>&nbsp;</div>
<div>Addresses delegated in Kolibri will not work, I had to skip them, work in progress</div>
<div>&nbsp;</div>
