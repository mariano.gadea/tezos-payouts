import requests, json, os, base58check, pymysql

def tezclient(comando):
	print(comando)
	cmd = 'export \'TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=Y\' &&'+ comando
	return os.popen(cmd).read()

def continua():
	resconti = input('Do you want to continue? (Y/n):')
	if not resconti:
		return True
	else:
		return False


#Variable definition

# red = 'http://alphanet-node.tzscan.io:80'  #alphanet
# red = 'http://zeronet-node.tzscan.io:80' #zeronet
# red = 'http://localhost:18732' #zeronet
red = 'http://localhost:8732' #network were we will inject operation
#redapi = 'http://api.zeronet.tzscan.io' zeronet
#redapi = 'https://api6.tzscan.io' #mainnet
redapi = 'https://api.tzkt.io' 
sourceaddress = 'tz3bEQoFCZEEfZMskefZ8q8e4eiHH1pssRax'
payaddress = 'tz1ceiboANJ72iqG1g9Xig8vW74JJijv4mb8'
payalias = 'ceibo'
tablasql = 'ceibo_pagos_betanet'
tablaint = 'ceibo_intereses_ciclo'
gas_limit = "15385"
fee = "1792"
storage_limit = "300"

#Establezco conexion con DB
conn=pymysql.connect(host='xxx.xxx.xx.xx',user='xxxxxxxx',password='xxxxxxxxx',database='xxxxxxxx')
#Busco el ciclo maximo
cursor = conn.cursor()

cursor.execute('select max(ciclo) from ' + tablasql)
dbquery = cursor.fetchone()
maxciclo = dbquery[0]

#Traigo los rewards a pagar
comision = 5
maxciclo += 1
ciclo = input('Insert cicle you want to pay: ('+str(maxciclo) +'): ')
if not ciclo:
	ciclo = maxciclo

url = requests.get(redapi + '/v1/rewards/split/'+ sourceaddress + '/' + str(ciclo)+'?limit=300')

rew = url.json()
stakingbalance = float(rew["stakingBalance"])
totalrew = float(rew["ownBlockRewards"]) + float(rew["extraBlockRewards"]) + float(rew["endorsementRewards"]) \
	+ float(rew["ownBlockFees"]) + float(rew["extraBlockFees"])

#Recupero hash del head actual
url = requests.get(red + "/chains/main/blocks/head/hash")
branchhash = url.json()

#recupero el counter de la cuenta
url = requests.get(red + "/chains/main/blocks/head/context/contracts/" + payaddress + "/counter")
counter = url.json()

#Recupero el protocolo
url = requests.get(red + "/chains/main/blocks/head/header/protocol_data")
protocol = url.json()["protocol"]



#Construyo el JSON
datajson = '{"contents": [] , "branch": "' + branchhash + '"}'
datajson = json.loads(datajson)
totapagar = 0 
totcomision = 0

sql = 'insert into ' + tablasql +' (ciclo,address,amount,comision,topay,txhash) values (%s, %s, %s, %s, %s, %s)'
print("          Address                       Balance           Reward    Baker Fee       To Pay  ")
print("--------------------------------------------------------------------------------------------")
for a in range(len(rew["delegators"])):
  d_address =rew["delegators"][a]["address"]
  d_balance=float(rew["delegators"][a]["balance"])
  d_reward=round(d_balance/stakingbalance*totalrew)
  d_comision=round(d_reward*(comision/100))
  d_apagar=d_reward-d_comision
  if (d_balance > 10000000 and d_address != 'KT1MAca3XdxAA9kW42gz4Sb6HvDmrM1xuyZQ'):
    counter = str(int(counter) + 1)
    print(d_address+' '+str.rjust(str(d_balance/1000000),14)+' '+str.rjust(str(d_reward/1000000),15)+' '+str.rjust(str(d_comision/1000000),10)+' '+str.rjust(str(d_apagar/1000000),13))
    datajson["contents"].append({'kind': 'transaction', 'amount': str(d_apagar), 'source': payaddress, 'destination': d_address, 'storage_limit': storage_limit, 'gas_limit': gas_limit,
    'fee': fee, 'counter': counter})
    cursor.execute(sql,(ciclo,d_address,d_reward,d_comision,d_apagar,'temp'))
    totapagar = totapagar + d_apagar
    totcomision = totcomision + d_comision
print("Total amount to pay: "+str(totapagar/1000000))
print("Total baker fee profit: "+str(totcomision/1000000))
print("Total earned: "+str(totalrew/1000000))
print("For the baker: "+str((totalrew-totapagar)/1000000))

if not continua():
	conn.rollback()
	conn.close()
	quit()

#Forge Operation
url = red + '/chains/main/blocks/head/helpers/forge/operations'
header = {'Cache-Control': 'no-cache','Content-Type': 'application/json'}
r = requests.post(url, data=json.dumps(datajson), headers=header)
operationbytes = r.json()
print(url)
print(datajson)


#Sign forged operation
edsi = tezclient('$HOME/tezos/tezos-client sign bytes 0x03' + operationbytes + ' for ' + payalias)[11:110]


print("Preaply......")
#Preapply Operation
url = red + '/chains/main/blocks/head/helpers/preapply/operations'
header = {'Cache-Control': 'no-cache','Content-Type': 'application/json'}
datajson2 = {'protocol': protocol , 'signature': edsi}
datajson.update(datajson2)
listdatajson = []
listdatajson.append(datajson)
r = requests.post(url, data=json.dumps(listdatajson), headers=header)
print(r.text)

if not continua():
	conn.rollback()
	conn.close()
	quit()


#Decode into hexadecimal format
edsihex = base58check.b58decode(edsi).hex()
signature = edsihex[10:-8]

#Inject operation
url = red + '/injection/operation?chain=main'
header = {'Cache-Control': 'no-cache','Content-Type': 'application/json'}
data = '"'+operationbytes+signature+'"'
r = requests.post(url, data=data, headers=header)
d_txhash =r.json()

#Commit en la base de datos
conn.commit()
sql = 'update ' + tablasql +' set txhash = %s where ciclo = %s'
cursor.execute(sql,(str(d_txhash),str(ciclo)))
conn.commit()
print('Operation was succesful! Operation Hash: '+str(d_txhash))

#Inserta el interes anual
d_ceibofee = comision / 100
d_realreward = totalrew - totalrew * d_ceibofee
d_rewardinpercentage = d_realreward * 100 / stakingbalance
d_rewardinpercentageperyear = d_rewardinpercentage * 128.3203


sql = 'insert into ' + tablaint +' (ciclo,stakingBalance,blocksRewards,endorsementsRewards,feesRewards, totalRewards,\
                ceiboFee,realReward,rewardInPercentage,rewardInPercentagePerYear) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'

cursor.execute(sql,(ciclo,stakingbalance,int(rew["ownBlockRewards"]) + int(rew["extraBlockRewards"]),\
	int(rew["endorsementRewards"]),int(rew["ownBlockFees"]) + int(rew["extraBlockFees"]),\
        int(totalrew),d_ceibofee,d_realreward, "{:.4f}".format(d_rewardinpercentage),"{:.4f}".format(d_rewardinpercentageperyear)))

print("Estimated Anual rate: "+str(d_rewardinpercentageperyear))

if not continua():
        conn.rollback()
        conn.close()
        quit()

#Commit en la base de datos
conn.commit()
print('Interes has been added to the database!')
conn.close()
